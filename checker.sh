#!/bin/bash
set -o nounset -o errexit -o pipefail
if [[ -f config ]]; then
  # Please set:
  # GITLAB_TOKEN (must have the api scope!)
  # CLONES_DIR
  # WEB_DIR
  source config
fi

gitlab_api() {
  curl -sSf \
    --request "${1}" \
    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    "https://gitlab.archlinux.org/api/v4/${2}" "${@:3}"
}

create_issue() {
  local project="${1}"
  local title="${2}"
  local description="${3}"

  gitlab_api POST "projects/${project}/issues" \
    --data-urlencode "title=${title}" \
    --data-urlencode "description=$(echo -e "${description}")" \
    >/dev/null
}

update_issue() {
  local project="${1}"
  local issue="${2}"
  local title="${3}"
  local description="${4}"

  gitlab_api PUT "projects/${project}/issues/${issue}" \
    --data-urlencode "title=${title}" \
    --data-urlencode "description=$(echo -e "${description}")" \
    >/dev/null
}

comment_on_issue() {
  local project="${1}"
  local issue="${2}"
  local body="${3}"

  gitlab_api POST "projects/${project}/issues/${issue}/notes" \
    --data-urlencode "body=$(echo -e "${body}")" \
    >/dev/null
}

check_package() {
  local package="${1}"
  local project="archlinux%2Fpackaging%2Fpackages%2F${package}"

  local last_check json
  last_check="$(date +%s)"
  # GIT_CONFIG_GLOBAL can be removed when [1] is released
  # https://gitlab.archlinux.org/archlinux/devtools/-/merge_requests/296
  json="$(GIT_CONFIG_GLOBAL=/dev/null pkgctl version check --json "${package}" | jq --compact-output first)"

  if ! jq --exit-status <<< "${json}" >/dev/null; then
    echo "No version status provided for ${package}"
    return
  fi

  if jq --exit-status '.status == "failure"' <<< "${json}" >/dev/null; then
    echo "Failure checking version status for ${package} ($(jq --raw-output .message <<< "${json}"))"
    return
  fi

  local local_version upstream_version
  local_version="$(jq --raw-output .local_version <<< "${json}")"
  upstream_version="$(jq --raw-output .upstream_version <<< "${json}")"
  if jq --exit-status '.out_of_date == false' <<< "${json}" >/dev/null; then
    issue="$(gitlab_api GET "/projects/${project}/issues?author_id=${author_id}&state=opened" | jq '.[].iid')"
    if [[ -n ${issue} ]]; then
      echo "${package}: current version ${local_version} is latest, closing existing issue (${issue})"

      comment_on_issue "${project}" "${issue}" "/close\nCurrent version ${local_version} is latest, closing the issue."
    fi
  else
    echo "${package}: upgrade from version ${local_version} to ${upstream_version}"

    title="New version: ${upstream_version}"
    description="/label ~status::confirmed\n/label ~scope::out-of-date\n\`\`\`json\n$(jq <<< "${json}")\n\`\`\`"
    issue="$(gitlab_api GET "/projects/${project}/issues?author_id=${author_id}&labels=scope::out-of-date&state=opened&search=New+version:&in=title" | jq '.[].iid')"
    if [[ -n ${issue} ]]; then
      upstream_version_in_issue="$(gitlab_api GET "/projects/${project}/issues?author_id=${author_id}&labels=scope::out-of-date&state=opened&search=New+version:&in=title" | jq -r '.[0].description | capture("upstream_version\": \"(?<version>[^\"]+)\"").version')"
      if [[ ${upstream_version_in_issue} != ${upstream_version} ]]; then
        update_issue "${project}" "${issue}" "${title}" "${description}"
        comment_on_issue "${project}" "${issue}" "Issue updated to match the latest ${upstream_version} upstream version."
      fi
    else
      create_issue "${project}" "${title}" "${description}"
    fi
  fi
  jq --compact-output --null-input --arg key "${package}" --argjson value "${json}" --arg last_check "${last_check}" '{$key: ($value + {"last_check": $last_check|tonumber})}' >> "${WEB_DIR}/.data.json"
}

check_packages() {
  # Generate the package list dynamically from https://archlinux.org/packages/pkgbase-maintainer
  mapfile -t packages < <(curl --location --show-error --no-progress-meter --fail --retry 3 --retry-delay 3 https://archlinux.org/packages/pkgbase-maintainer | jq --raw-output --exit-status 'to_entries | map(select(.value | any(. as $item | ["Antiz", "gromit", "Segaja"] | index($item)))) | .[].key')

  echo -n > "${WEB_DIR}/.data.json"
  for package in "${packages[@]}"; do
    if ! check_package "${package}"; then
      echo "Error checking package ${package}"
    fi
  done
  local json
  json="$(jq --compact-output --slurp '[.[][]] | map( { (.pkgbase|tostring): . } ) | add' < "${WEB_DIR}/.data.json")"
  if [[ -f ${WEB_DIR}/data.json ]]; then
    cat "${WEB_DIR}/data.json" <(echo "${json}") | jq --compact-output --slurp add > "${WEB_DIR}/.data.json"
  else
    echo "${json}" > "${WEB_DIR}/.data.json"
  fi
  mv "${WEB_DIR}/"{.,}data.json
}

author_id="$(gitlab_api GET "user" | jq .id)"
cd "${CLONES_DIR}"
check_packages
