#!/usr/bin/env -S python -u
import datetime
import os
import pathlib
import re
import shutil
import subprocess

import requests


def package_name_to_project_name(package):
    # https://gitlab.archlinux.org/archlinux/devtools/-/blob/1101de9fb93dc9ce556681666fc4d24856e91804/src/lib/api/gitlab.sh#L336-L355
    package = re.sub(r"([a-zA-Z0-9]+)\+([a-zA-Z]+)", r"\1-\2", package)
    package = re.sub(r"\+", r"plus", package)
    package = re.sub(r"[^a-zA-Z0-9_\-\.]", r"-", package)
    package = re.sub(r"[_\-]{2,}", r"-", package)
    package = re.sub(r"^tree$", r"unix-tree", package)
    return package


if "CLONES_DIR" not in os.environ:
    print("CLONES_DIR must be set!")
    exit(1)

clones_dir = os.environ.get("CLONES_DIR")

r = requests.get("https://archlinux.org/packages/pkgbase-maintainer")
r.raise_for_status()
pkgbases = r.json().keys()

existing_repos = [c.name for c in pathlib.Path(clones_dir).iterdir() if c.is_dir()]
missing_repos = list(set(pkgbases) - set(existing_repos))
inactive_repos = list(set(existing_repos) - set(pkgbases))

print(f"{len(inactive_repos)} inactive repos")
for repo in inactive_repos:
    shutil.rmtree(f"{clones_dir}/{repo}")

print(f"{len(missing_repos)} missing repos")
for repo in missing_repos:
    subprocess.run(
        [
            "git",
            "-C",
            clones_dir,
            "clone",
            f"https://gitlab.archlinux.org/archlinux/packaging/packages/{package_name_to_project_name(repo)}.git",
            repo,
        ],
        check=True,
    )

current_run = datetime.datetime.now(datetime.timezone.utc)
last_run = datetime.datetime.fromtimestamp(
    int(pathlib.Path(f"{clones_dir}/last_run").read_text()), datetime.timezone.utc
)
# https://gitlab.com/gitlab-org/gitlab/-/blob/f9e97d2e30940234341f74ecf318a39911403972/app/models/event.rb#L381
# https://gitlab.com/gitlab-org/gitlab/-/blob/f9e97d2e30940234341f74ecf318a39911403972/app/models/event.rb#L463-L465
last_run -= datetime.timedelta(hours=1, minutes=1)

next = "https://gitlab.archlinux.org/api/v4/groups/archlinux%2Fpackaging%2Fpackages/projects?pagination=keyset&per_page=100&order_by=last_activity_at&simple=true"

updated_repos = []
while True:
    r = requests.get(next)
    r.raise_for_status()
    for project in r.json():
        if project["name"] not in pkgbases:
            continue

        last_activity_at = datetime.datetime.fromisoformat(project["last_activity_at"])
        if last_activity_at < last_run:
            break
        updated_repos.append(project["name"])
    else:
        next = r.links["next"]["url"]
        continue
    break

print(f"{len(updated_repos)} repos with activity since last run")
for repo in updated_repos:
    subprocess.run(
        ["git", "-C", f"{clones_dir}/{repo}", "pull"],
        check=True,
    )

pathlib.Path(f"{clones_dir}/last_run").write_text(
    str(int(current_run.timestamp())) + "\n"
)
